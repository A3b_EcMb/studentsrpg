﻿using Level.LevelProgress;

namespace LocalMessages
{
    public struct StageCompletedMessage
    {
        public LevelProgressStage Stage;
    }
}