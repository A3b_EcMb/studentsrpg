﻿using System.Collections.Generic;
using Invector.vItemManager;
using LocalMessages;
using Newtonsoft.Json;
using Services;
using Services.DI;
using UnityEngine;

namespace PlayerProgress
{
    public class PlayerProgressManager
    {
        private const string ProgressKey = "PlayerProgress";

        public bool IsLastLevel => Current.level >= LevelsConfig.levels.Count - 1;
        public LevelInfo NextLevel => IsLastLevel ? LevelsConfig.levels[^1] : LevelsConfig.GetNext(Current.level);
        public PlayerProgress Current { get; private set; }
        public PlayerLevelsConfig LevelsConfig { get; private set; }

        [Inject] private MessageBroker _messageBroker;

        public void Initialize(PlayerLevelsConfig levelsConfig)
        {
            _messageBroker.Subscribe<EnemyDeadMessage>(OnEnemyDead);
            LevelsConfig = levelsConfig;
            
            if (!PlayerPrefs.HasKey(ProgressKey))
            {
                Current = new PlayerProgress
                {
                    openedStage = 1,
                    ownedItems = new List<OwnedItem>()
                };
                return;
            }

            try
            {
                string json = PlayerPrefs.GetString(ProgressKey);
                Current = JsonConvert.DeserializeObject<PlayerProgress>(json);
                Current.ownedItems ??= new List<OwnedItem>();
            }
            catch
            {
                Current = new PlayerProgress
                {
                    openedStage = 1,
                    ownedItems = new List<OwnedItem>()
                };
            }
        }

        public void CheckMinOpenedStage()
        {
            if (Current.openedStage >= 1) return;

            Current.openedStage = 1;
            Save();
        }

        public void ReadItemsFromInventory(vItemManager itemManager)
        {
            Current.ownedItems.Clear();
            foreach (var item in itemManager.items)
            {
                Current.ownedItems.Add(new OwnedItem
                {
                    id = item.id,
                    count = item.amount,
                });
            }
            
            Save();
        }
        
        public void Save()
        {
            string json = JsonConvert.SerializeObject(Current);
            PlayerPrefs.SetString(ProgressKey, json);
        }

        private void OnEnemyDead(ref EnemyDeadMessage message)
        {
            if (IsLastLevel)
                return;
            
            Current.exp += LevelsConfig.expForKill;
            if (Current.exp < NextLevel.expToAchieve) return;

            Current.exp -= NextLevel.expToAchieve;
            Current.level++;
            Save();
        }
    }
}