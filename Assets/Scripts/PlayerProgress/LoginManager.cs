﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Google;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using UnityEngine;

namespace PlayerProgress
{
    public struct LoginResult
    {
        public bool success;
        public int stage;
    }
    
    public class LoginManager
    {
        public bool IsAuthorized { get; private set; }
        
        private const string ApiKey = "AIzaSyDoJcbD7ojntJ5P1ONAUCjpCOxk7xeLO8M";
        private const string AppName = "Ansalonia";
        private const string SpreadSheetId = "1g5T4X5k9xmsvR5JTOhthWLV4sLrkdKWsZkYLix8IyoA";
        private const string Range = "MainPage!A2:Z";

        private const string CredentialsPrefsLoginKey = "PlayerCredentialsLogin";
        private const string CredentialsPrefsPasswordKey = "PlayerCredentialsPassword";
        
        private readonly SheetsService _service = new(new BaseClientService.Initializer
        {
            ApiKey = ApiKey,
            ApplicationName = AppName
        });

        private Action<LoginResult> _callback;
        
        public void Login(string login, string password, Action<LoginResult> callback)
        {
            _callback = callback;
            LoginAsync(login, password).Forget();
        }

        public void Logout()
        {
            IsAuthorized = false;
            PlayerPrefs.DeleteKey(CredentialsPrefsLoginKey);
            PlayerPrefs.DeleteKey(CredentialsPrefsPasswordKey);
        }

        public async UniTask<LoginResult> LoginWithSavedCreds()
        {
            if (!PlayerPrefs.HasKey(CredentialsPrefsLoginKey) || !PlayerPrefs.HasKey(CredentialsPrefsPasswordKey))
            {
                return new LoginResult
                {
                    success = false
                };
            }
            
            string login = PlayerPrefs.GetString(CredentialsPrefsLoginKey);
            string password = PlayerPrefs.GetString(CredentialsPrefsPasswordKey);

            var result = await ProcessLoginAsync(login, password);
            
            IsAuthorized = result.success;
            return result;
        }

        private async UniTask LoginAsync(string login, string password)
        {
            var result = await ProcessLoginAsync(login, password);
            await UniTask.SwitchToMainThread();

            IsAuthorized = result.success;
            _callback?.Invoke(result);
        }

        private async UniTask<LoginResult> ProcessLoginAsync(string login, string password)
        {
            var request = _service.Spreadsheets.Values.Get(SpreadSheetId, Range);
            ValueRange response;
            try
            {
                response = await request.ExecuteAsync();
            }
            catch (GoogleApiException e)
            {
                Debug.LogError("Cannot login to google:");
                Debug.LogException(e);
                return new LoginResult
                {
                    success = false
                };
            }
            
            var values = response.Values;
            if (values == null || values.Count == 0)
            {
                return new LoginResult
                {
                    success = false
                };
            }

            foreach (var row in values)
            {
                if (row.Count < 4) continue;
                if (row[2].ToString() != login) continue;
                if (row[3].ToString() != password) continue;

                int stage = GetOpenedStageFromRow(row);
                SaveCredsToPrefs(login, password);
                return new LoginResult
                {
                    success = true,
                    stage = stage,
                };
            }
            
            return new LoginResult
            {
                success = false
            };
        }

        private void SaveCredsToPrefs(string login, string password)
        {
            PlayerPrefs.SetString(CredentialsPrefsLoginKey, login);
            PlayerPrefs.SetString(CredentialsPrefsPasswordKey, password);
        }

        private int GetOpenedStageFromRow(IList<object> row)
        {
            if (row.Count <= 4)
                return 1;

            int stage = 1;
            for (int i = 4; i < row.Count; i++)
            {
                if (!row[i].ToString().Equals("TRUE"))
                    break;

                stage = i - 2;
            }
            
            return stage;
        }
    }
}