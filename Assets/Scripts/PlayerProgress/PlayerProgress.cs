﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PlayerProgress
{
    [JsonObject]
    public class PlayerProgress
    {
        [JsonProperty("level")] public int level;
        [JsonProperty("exp")] public int exp;
        [JsonProperty("completed_stage")] public int completedStage;
        [JsonProperty("last_seen_opened_stage")] public int lastSeenOpenedStage;
        [JsonProperty("owned_items")] public List<OwnedItem> ownedItems;
        
        [JsonIgnore] public int openedStage;
    }

    [JsonObject]
    public class OwnedItem
    {
        [JsonProperty("id")] public int id;
        [JsonProperty("count")] public int count;
    }
}