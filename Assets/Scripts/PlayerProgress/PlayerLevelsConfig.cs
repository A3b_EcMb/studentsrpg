﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerProgress
{
    [Serializable]
    public class LevelInfo
    {
        [SerializeField] public int expToAchieve;
    }
    
    [CreateAssetMenu(menuName = "Progress/Levels Config")]
    public class PlayerLevelsConfig : ScriptableObject
    {
        [SerializeField] public int expForKill;
        [SerializeField] public int expForOpenStage;
        [SerializeField] public List<LevelInfo> levels;

        public LevelInfo GetNext(int level) => levels[level + 1];
    }
}