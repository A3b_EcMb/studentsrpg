﻿using Cysharp.Threading.Tasks;
using Level.Respawn;
using Services.DI;
using Services.WindowsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class RespawnWindow : WindowBase
    {
        [SerializeField] private Button _confirmButton;
        [SerializeField] private Button _menuButton;

        [Inject] private RespawnManager _respawnManager;
        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private Startup.Startup _startup;

        private void Start()
        {
            _confirmButton.onClick.AddListener(() =>
            {
                _windowsSystem.DestroyWindow(this);
                _respawnManager.ConfirmRespawn();
            });
            _menuButton.onClick.AddListener(() =>
            {
                _windowsSystem.DestroyWindow(this);
                _startup.GoToMenu().Forget();
            });
        }
    }
}