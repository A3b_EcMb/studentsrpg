﻿using PlayerProgress;
using Services.DI;
using Services.WindowsSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class LoginWindow : WindowBase
    {
        [SerializeField] private GameObject _loginErrorText;
        [SerializeField] private GameObject _connectingText;
        [SerializeField] private TMP_InputField _loginInput;
        [SerializeField] private TMP_InputField _passwordInput;
        [SerializeField] private Button _loginButton;
        [SerializeField] private Button _settingsButton;

        [Inject] private LoginManager _loginManager;
        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private PlayerProgressManager _progressManager;

        private void Start()
        {
            _loginButton.onClick.AddListener(Login);
            _settingsButton.onClick.AddListener(OpenSettings);
            
            _loginErrorText.SetActive(false);
            _connectingText.SetActive(false);
        }

        private void Login()
        {
            _loginErrorText.SetActive(false);
            _connectingText.SetActive(true);

            string login = _loginInput.text;
            string password = _passwordInput.text;

            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
            {
                _loginErrorText.SetActive(true);
                return;
            }
            
            _loginManager.Login(login, password, OnLoginResults);
        }

        private void OnLoginResults(LoginResult result)
        {
            if (!result.success)
            {
                _connectingText.SetActive(false);
                _loginErrorText.SetActive(true);
                return;
            }

            _progressManager.Current.openedStage = result.stage;
            _windowsSystem.CreateWindow<MainMenu>();
            _windowsSystem.DestroyWindow(this);
        }

        private void OpenSettings()
        {
            _windowsSystem.CreateWindow<SettingsWindow>();
        }
    }
}