﻿using Cysharp.Threading.Tasks;
using PlayerProgress;
using Services.DI;
using Services.WindowsSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class MainMenu : WindowBase
    {
        [SerializeField] private Button _clearProgressButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _logoutButton;
        [SerializeField] private TextMeshProUGUI _currentLevelText;
        [SerializeField] private TextMeshProUGUI _currentExpText;
        [SerializeField] private Slider _expSlider;
        [SerializeField] private GameObject _newStageOpenedLabel;
        
        [Inject] private Startup.Startup _startup;
        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private PlayerProgressManager _progressManager;
        [Inject] private LoginManager _loginManager;
 
        private void Start()
        {
            _settingsButton.onClick.AddListener(OpenSettings);
            _playButton.onClick.AddListener(Play);
            _logoutButton.onClick.AddListener(Logout);

#if UNITY_EDITOR
            _clearProgressButton.onClick.AddListener(() =>
            {
                _progressManager.Current.lastSeenOpenedStage = 1;
                _progressManager.Current.completedStage = 0;
                _progressManager.Current.ownedItems.Clear();
                _progressManager.Current.level = 0;
                _progressManager.Current.exp = 0;
                _progressManager.Save();
            });
#else
            _clearProgressButton.gameObject.SetActive(false);
#endif

            var progress = _progressManager.Current;
            _currentLevelText.text = progress.level.ToString();
            _currentExpText.text = _progressManager.IsLastLevel
                ? progress.exp.ToString()
                : $"{progress.exp.ToString()}/{_progressManager.NextLevel.expToAchieve.ToString()}";
            
            _expSlider.value = _progressManager.IsLastLevel
                ? 1f
                : progress.exp * 1f / _progressManager.NextLevel.expToAchieve;

            _newStageOpenedLabel.SetActive(_progressManager.Current.lastSeenOpenedStage < _progressManager.Current.openedStage);
            _progressManager.Current.lastSeenOpenedStage = _progressManager.Current.openedStage;
            _progressManager.Save();
        }

        private void Logout()
        {
            _loginManager.Logout();
            _windowsSystem.CreateWindow<LoginWindow>();
            _windowsSystem.DestroyWindow(this);
        }

        private void OpenSettings()
        {
            _windowsSystem.CreateWindow<SettingsWindow>();
        }

        private void Play()
        {
            _windowsSystem.DestroyWindow(this);
            _startup.Play().Forget();
        }
    }
}