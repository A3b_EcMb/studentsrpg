﻿using Services.DI;
using Services.WindowsSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class PopupWindow : WindowBase
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Button _closeButton;

        [Inject] private WindowsSystem _windowsSystem;

        private void Start()
        {
            _closeButton.onClick.AddListener(() => _windowsSystem.DestroyWindow(this));
        }

        public void Setup(string text)
        {
            _text.text = text;
        }
    }
}