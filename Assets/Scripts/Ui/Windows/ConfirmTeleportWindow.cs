﻿using Level;
using Services.DI;
using Services.WindowsSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class ConfirmTeleportWindow : WindowBase
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Button _confirmButton;
        [SerializeField] private Button _cancelButton;

        [Inject] private WindowsSystem _windowsSystem;

        private TeleportPoint _point;
        
        private void Awake()
        {
            _confirmButton.onClick.AddListener(Confirm);
            _cancelButton.onClick.AddListener(Close);
        }

        public void Setup(LocationType targetLocation, TeleportPoint point)
        {
            _point = point;
            _text.text = targetLocation == LocationType.Caves ? "Перейти в пещеры?" : "Вернуться в город?";
        }

        private void Confirm()
        {
            if (_point != null)
                _point.ConfirmTeleport();
            
            Close();
        }

        private void Close()
        {
            _windowsSystem.DestroyWindow(this);
        }
    }
}