﻿using System;
using GameCamera;
using Services.DI;
using Services.WindowsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class SkipCutsceneWindow : WindowBase
    {
        public CameraCutsceneManager cutsceneManager;
        
        [SerializeField] private Button _skipButton;

        [Inject] private WindowsSystem _windowsSystem;

        private void Start()
        {
            _skipButton.onClick.AddListener(() => cutsceneManager.Finish());
        }
    }
}