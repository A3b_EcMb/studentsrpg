﻿using System.Text;
using Level.Respawn;
using Services.DI;
using Services.WindowsSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Windows
{
    public class InGameUI : WindowBase
    {
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Slider _hpSlider;
        [SerializeField] private TextMeshProUGUI _hpText;

        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private RespawnManager _respawnManager;

        private int _currentHp;
        private int _maxHp;

        private StringBuilder _stringBuilder;

        private void Start()
        {
            _stringBuilder = new StringBuilder();
            _pauseButton.onClick.AddListener(Pause);
        }

        private void Update()
        {
            var playerHp = _respawnManager.PlayerHealthController;
            if (playerHp == null) return;

            _hpSlider.value = playerHp.currentHealth / playerHp.maxHealth;

            int currentHp = Mathf.FloorToInt(playerHp.currentHealth);
            int maxHp = Mathf.FloorToInt(playerHp.maxHealth);
            
            if (currentHp == _currentHp && maxHp == _maxHp) return;

            _stringBuilder.Clear();
            _stringBuilder.Append(currentHp);
            _stringBuilder.Append("/");
            _stringBuilder.Append(maxHp);
            _hpText.text = _stringBuilder.ToString();

            _currentHp = currentHp;
            _maxHp = maxHp;
        }

        private void Pause()
        {
            _windowsSystem.CreateWindow<PauseWindow>();
        }
    }
}