﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Services
{
    public static class Extensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void MoveToLocalZero(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
    }
}