using Invector.vCharacterController;
using UnityEngine;
using UnityEngine.EventSystems;
namespace UnityStandardAssets.CrossPlatformInput
{
    public class MoveCamera : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        protected string cameraAxisX = "Mouse X";
        [SerializeField]
        protected string cameraAxisY = "Mouse Y";
        [SerializeField]
        protected float _sensitivityX = 1f;
        [SerializeField]
        protected float _sensitivityY = 1f;
        [SerializeField]
        protected RectTransform touchPointer;

        private CrossPlatformInputManager.VirtualAxis m_CameraX_VirtualAxis; // Reference to the joystick in the cross platform input
        private CrossPlatformInputManager.VirtualAxis m_CameraY_VirtualAxis; // Reference to the joystick in the cross platform input

        public bool touchPosToMousePos;
        private Vector2 TouchDirection { get; set; }
        private Vector2 PreviousTouchPosition { get; set; }
        private int CurrentPointerID { get; set; }
        private bool IsPressed { get; set; }

        private float SensitivityY { get; set; }
        private float SensitivityX { get; set; }

        private void Start()
        {
            CreateVirtualAxes();
            if (touchPointer)
                touchPointer.gameObject.SetActive(false);

            SensitivityX = 1;
            SensitivityY = 1;
        }

        private void CreateVirtualAxes()
        {
            // create new axes based on axes to use
            if (CrossPlatformInputManager.AxisExists(cameraAxisX))
            {
                m_CameraX_VirtualAxis = CrossPlatformInputManager.VirtualAxisReference(cameraAxisX);
            }
            else
            {
                m_CameraX_VirtualAxis = new CrossPlatformInputManager.VirtualAxis(cameraAxisX);
                CrossPlatformInputManager.RegisterVirtualAxis(m_CameraX_VirtualAxis);
            }
            
            if (CrossPlatformInputManager.AxisExists(cameraAxisY))
            {
                m_CameraY_VirtualAxis = CrossPlatformInputManager.VirtualAxisReference(cameraAxisY);
            }
            else
            {
                m_CameraY_VirtualAxis = new CrossPlatformInputManager.VirtualAxis(cameraAxisY);
                CrossPlatformInputManager.RegisterVirtualAxis(m_CameraY_VirtualAxis);
            }
        }

        private void Update()
        {
            HandleTouchDirection();
        }

        private void HandleTouchDirection()
        {
            if (!IsPressed) return;
            
            vMousePositionHandler.Instance.clampScreen = !touchPosToMousePos;
            if (CurrentPointerID >= 0 && CurrentPointerID < Input.touches.Length)
            {                    
                var touchPos = Input.touches[CurrentPointerID].position;
                if (touchPosToMousePos)
                    vMousePositionHandler.Instance.joystickMousePos = touchPos;

                TouchDirection = touchPos - PreviousTouchPosition;
                if (touchPointer)
                    touchPointer.position = touchPos;

                PreviousTouchPosition = Input.touches[CurrentPointerID].position;
            }
            else
            {
                Vector2 touchPos = Input.mousePosition;
                if (touchPosToMousePos)
                    vMousePositionHandler.Instance.joystickMousePos = touchPos;

                TouchDirection = touchPos - PreviousTouchPosition;
                if (touchPointer)
                    touchPointer.position = touchPos;

                PreviousTouchPosition = Input.mousePosition;
            }

            UpdateVirtualAxes(new Vector3(TouchDirection.x * _sensitivityX * SensitivityX, TouchDirection.y * _sensitivityY * SensitivityY, 0));
        }

        private void UpdateVirtualAxes(Vector3 value)
        {
            m_CameraX_VirtualAxis.Update(value.x);
            m_CameraY_VirtualAxis.Update(value.y);
        }

        public void OnPointerDown(PointerEventData data)
        {
            if (touchPointer)
            {
                touchPointer.position = data.position;
                touchPointer.gameObject.SetActive(true);
            }
            
            IsPressed = true;
            CurrentPointerID = data.pointerId;
            PreviousTouchPosition = data.position;
        }

        public void OnPointerUp(PointerEventData data)
        {
            if (touchPointer)
            {
                touchPointer.position = data.position;
                touchPointer.gameObject.SetActive(false);
            }
            
            IsPressed = false;
            TouchDirection = Vector2.zero;
            UpdateVirtualAxes(Vector3.zero);
        }
    }
}