﻿using System;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace GameCamera
{
    public class CameraCutsceneManager : MonoBehaviour
    {
        public Camera MainCamera { get; private set; }
        
        [SerializeField] private Camera _cutsceneCamera;

        [Inject] private WindowsSystem _windowsSystem;
        
        private bool _followingPath;
        private float _timer;
        private CameraCutscene _cutscene;

        private Action _finishedCallback;

        private void Start()
        {
            GameContainer.InjectToInstance(this);
        }

        private void Update()
        {
            if (!_followingPath) return;

            _timer -= Time.deltaTime;
            if (_timer > 0f) return;

            Finish();
        }

        public void StartCutscene(CameraCutscene cutscene, Action finishedCallback = null)
        {
            _cutscene = cutscene;
            _finishedCallback = finishedCallback;
            _followingPath = true;
            _timer = cutscene.CutsceneTime + cutscene.ExtraTimeAtEnd;

            cutscene.Path.selectedCamera = _cutsceneCamera;
            cutscene.Path.PlayPath(cutscene.CutsceneTime);

            MainCamera = Camera.main;
            
            _cutsceneCamera.gameObject.SetActive(true);
            MainCamera.gameObject.SetActive(false);

            if (_windowsSystem.TryGetWindow(out InGameUI inGameUI))
                inGameUI.gameObject.SetActive(false);

            var skipWindow = _windowsSystem.CreateWindow<SkipCutsceneWindow>();
            skipWindow.cutsceneManager = this;
        }

        public void Finish()
        {
            _timer = 0f;
            _followingPath = false;
            _finishedCallback?.Invoke();
            
            _cutscene.Path.StopPath();
            _cutsceneCamera.gameObject.SetActive(false);
            MainCamera.gameObject.SetActive(true);
            
            if (_windowsSystem.TryGetWindow(out InGameUI inGameUI))
                inGameUI.gameObject.SetActive(true);
            
            _windowsSystem.DestroyWindow<SkipCutsceneWindow>();
        }
    }
}