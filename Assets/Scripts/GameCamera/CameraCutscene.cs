﻿using System;
using UnityEngine;

namespace GameCamera
{
    [Serializable]
    public class CameraCutscene
    {
        [SerializeField] public float CutsceneTime;
        [SerializeField] public float ExtraTimeAtEnd;
        [SerializeField] public CPC_CameraPath Path;
    }
}