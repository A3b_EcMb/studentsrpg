﻿using System;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace Level
{
    public enum LocationType
    {
        City,
        Caves,
    }
    
    public class TeleportPoint : MonoBehaviour
    {
        [SerializeField] private LocationType _targetLocation;
        [SerializeField] private TeleportPoint _targetPoint;
        [SerializeField] private Transform _exitPoint;
        
        [Inject] private WindowsSystem _windowsSystem;
        private Transform _player;

        private void Awake()
        {
            GameContainer.InjectToInstance(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.attachedRigidbody.CompareTag("Player")) return;

            _player = other.attachedRigidbody.transform;
            var teleportWindow = _windowsSystem.CreateWindow<ConfirmTeleportWindow>();
            teleportWindow.Setup(_targetLocation, this);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.attachedRigidbody.CompareTag("Player")) return;
            
            _windowsSystem.DestroyWindow<ConfirmTeleportWindow>();
            _player = null;
        }

        public void ConfirmTeleport()
        {
            if (_player == null) return;
            
            _player.SetPositionAndRotation(_targetPoint._exitPoint.position, _targetPoint._exitPoint.rotation);
            _player = null;
        }
    }
}