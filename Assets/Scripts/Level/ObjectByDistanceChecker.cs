﻿using Services.DI;
using UnityEngine;

namespace Level
{
    public class ObjectByDistanceChecker : MonoBehaviour
    {
        [Inject] private ObjectsOnDistanceDisabler _disabler;

        public void Enable()
        {
            GameContainer.InjectToInstance(this);
            _disabler.Register(transform);
        }

        private void OnDestroy()
        {
            _disabler.Unregister(transform);
        }
    }
}