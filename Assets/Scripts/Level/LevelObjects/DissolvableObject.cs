﻿using UnityEngine;

namespace Level
{
    public class DissolvableObject : BaseDestroyableObject
    {
        private static readonly int Dissolve = Shader.PropertyToID("_Dissolve");
        
        [SerializeField] private float _dissolveTime;
        [SerializeField] private Renderer _renderer;

        private bool _started;
        private float _timer;

        private void Update()
        {
            if (!_started) return;
            
            foreach (var material in _renderer.materials)
            {
                material.SetFloat(Dissolve, 1f - _timer / _dissolveTime);                
            }
            _timer -= Time.deltaTime;
            
            if (_timer <= 0f)
                Destroy(gameObject);
        }

        public override void Destroy()
        {
            _started = true;
            _timer = _dissolveTime;
        }
    }
}