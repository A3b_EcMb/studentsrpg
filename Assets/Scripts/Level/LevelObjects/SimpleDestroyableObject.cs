﻿namespace Level
{
    public class SimpleDestroyableObject : BaseDestroyableObject
    {
        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}