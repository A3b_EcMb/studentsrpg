﻿using System.Linq;
using Invector.vItemManager;
using PlayerProgress;
using Services.DI;
using UnityEngine;

namespace Level
{
    public class CollectableObject : MonoBehaviour
    {
        [SerializeField] private vItemCollection _itemCollection;

        [Inject] private PlayerProgressManager _progressManager;
        
        private void Awake()
        {
            GameContainer.InjectToInstance(this);

            int id = _itemCollection.items[0].id;
            if (_progressManager.Current.ownedItems.All(x => x.id != id)) return;
            
            Debug.Log($"Collected weapons contains {id}, destroying");
            gameObject.SetActive(false);
        }
    }
}