﻿using UnityEngine;

namespace Level
{
    public abstract class BaseDestroyableObject : MonoBehaviour
    {
        public abstract void Destroy();
    }
}