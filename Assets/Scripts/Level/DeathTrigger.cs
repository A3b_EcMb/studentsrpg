﻿using Invector;
using UnityEngine;

namespace Level
{
    public class DeathTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var health = other.GetComponentInChildren<vHealthController>();
            if (health == null) return;
            
            health.TakeDamage(new vDamage(100000000, true));
        }
    }
}