﻿using GameCamera;
using UnityEngine;

namespace Level
{
    public class CutsceneTrigger : MonoBehaviour
    {
        [SerializeField] private CameraCutsceneManager _cutsceneManager;
        [SerializeField] private CameraCutscene _cutscene;

        private bool _played;

        private void OnTriggerEnter(Collider other)
        {
            if (_played) return;
            if (!other.attachedRigidbody.CompareTag("Player")) return;
            
            _cutsceneManager.StartCutscene(_cutscene);
            _played = true;
        }
    }
}