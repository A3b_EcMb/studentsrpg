﻿using System;
using Invector;
using Services;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace Level
{
    public class FinalMessageTrigger : MonoBehaviour
    {
        [SerializeField] private vHealthController _boss;

        [Inject] private WindowsSystem _windowsSystem;
        
        private bool _bossIsDead;
        
        private void Start()
        {
            GameContainer.InjectToInstance(this);
            _boss.onDead.AddListener(OnBossDead);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_bossIsDead) return;
            if (!other.attachedRigidbody.CompareTag("Player")) return;
            
            _windowsSystem.CreateWindow<PopupWindow>()
                .Setup(GameConst.GameFinishedText);
        }

        private void OnBossDead(GameObject arg0)
        {
            _bossIsDead = true;
        }
    }
}