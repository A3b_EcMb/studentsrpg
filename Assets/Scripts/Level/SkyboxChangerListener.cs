﻿using Invector;
using UnityEngine;

namespace Level
{
    public class SkyboxChangerListener : MonoBehaviour
    {
        [SerializeField] private vHealthController _targetEnemy;
        [SerializeField] private Material _skyboxMaterial;

        private void Start()
        {
            _targetEnemy.onDead.AddListener(OnEnemyDead);
        }

        private void OnEnemyDead(GameObject enemy)
        {
            RenderSettings.skybox = _skyboxMaterial;
        }
    }
}