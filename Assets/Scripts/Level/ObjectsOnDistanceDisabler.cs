﻿using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class ObjectsOnDistanceDisabler : MonoBehaviour
    {
        [SerializeField] private float _disableDistance;
        
        private List<Transform> _targets = new List<Transform>(50);

        private float _timer;

        private void Update()
        {
            _timer += Time.deltaTime;
            if (_timer < 1f) return;

            if (Camera.main == null) return;
            
            var cameraPosition = Camera.main.transform.position;
            foreach (var target in _targets)
            {
                float distance = Vector3.Distance(target.position, cameraPosition);
                if (distance > _disableDistance && target.gameObject.activeSelf)
                    target.gameObject.SetActive(false);
                else if (distance <= _disableDistance && !target.gameObject.activeSelf)
                    target.gameObject.SetActive(true);
            }
        }

        public void Register(Transform target)
        {
            if (!_targets.Contains(target))
                _targets.Add(target);
        }

        public void Unregister(Transform target)
        {
            _targets.Remove(target);
        }
    }
}