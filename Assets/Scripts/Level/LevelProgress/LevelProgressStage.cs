﻿using System;
using System.Collections.Generic;
using GameCamera;
using Invector;
using Level.Citizens;
using LocalMessages;
using Services;
using Services.DI;
using UnityEngine;

namespace Level.LevelProgress
{
    public class LevelProgressStage : MonoBehaviour
    {
        public bool Active { get; set; }
        public bool Completed { get; set; }
        public int Id { get; set; }
        
        public event Action<LevelProgressStage> OnStageCompleted;

        [SerializeField] public bool ShowClearedPopup;
        [SerializeField] public bool UseAsStartSpawn;
        [SerializeField] public List<BaseDestroyableObject> ObjectsToDestroyOnOpen;
        [SerializeField] public List<BaseDestroyableObject> ObjectsToDestroyOnComplete;
        [SerializeField] public List<GameObject> ObjectsToEnableOnComplete;
        [SerializeField] public CitizensWaypointsGroup CitizensGroup;
        [SerializeField] public List<vHealthController> EnemiesToDefeat;
        [SerializeField] public CameraCutscene Cutscene;
        [SerializeField] public LevelProgressStage Next;
        [SerializeField] public Transform SpawnPoint;

        [Inject] private MessageBroker _messageBroker;

        private int _remainingEnemies;
        private bool _initialized;
        
        public void Init()
        {
            GameContainer.InjectToInstance(this);
            
            _remainingEnemies = EnemiesToDefeat.Count;
            foreach (var enemy in EnemiesToDefeat)
            {
                enemy.OnDead += OnEnemyDead;
                enemy.gameObject.SetActive(false);
            }
            
            foreach (var objectToEnable in ObjectsToEnableOnComplete)
            {
                objectToEnable.SetActive(false);
            }

            _initialized = true;
        }

        public void Show()
        {
            if (Completed) return;
            if (Active) return;
            if (!_initialized) return;
            
            foreach (var objectToDestroy in ObjectsToDestroyOnOpen)
            {
                if (objectToDestroy != null)
                    objectToDestroy.Destroy();
            }
            
            foreach (var enemy in EnemiesToDefeat)
            {
                enemy.gameObject.SetActive(true);
            }

            Active = true;
        }

        private void OnEnemyDead(GameObject target)
        {
            var message = new EnemyDeadMessage();
            _messageBroker.Trigger(ref message);
            _remainingEnemies--;
            
            if (_remainingEnemies <= 0)
                Complete();
        }

        public void Complete()
        {
            foreach (var objectToDestroy in ObjectsToDestroyOnOpen)
            {
                if (objectToDestroy != null)
                    objectToDestroy.Destroy();
            }
            
            foreach (var objectToDestroy in ObjectsToDestroyOnComplete)
            {
                objectToDestroy.Destroy();
            }

            foreach (var objectToEnable in ObjectsToEnableOnComplete)
            {
                objectToEnable.SetActive(true);
            }
            
            if (CitizensGroup != null)
                CitizensGroup.Spawn();

            foreach (var enemy in EnemiesToDefeat)
            {
                Destroy(enemy.gameObject);
            }
            
            OnStageCompleted?.Invoke(this);
            Completed = true;
            Active = false;
        }
    }
}