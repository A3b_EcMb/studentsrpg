﻿using System.Collections.Generic;
using GameCamera;
using LocalMessages;
using PlayerProgress;
using Services;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace Level.LevelProgress
{
    public class LevelProgressManager : MonoBehaviour
    {
        public List<LevelProgressStage> Stages => _stages;
        
        [SerializeField] private CameraCutsceneManager _cutsceneManager;
        [SerializeField] private List<LevelProgressStage> _stages;

        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private PlayerProgressManager _progressManager;
        [Inject] private MessageBroker _messageBroker;

        public void Initialize()
        {
            GameContainer.InjectToInstance(this);

            for (int i = 0; i < _stages.Count; i++)
            {
                var stage = _stages[i];
                stage.Id = i + 1;
                stage.Init();
            }
            
            foreach (var stage in _stages)
            {
                if (stage.Id <= _progressManager.Current.completedStage)
                {
                    stage.Complete();
                    continue;
                }
                
                if (stage.Id > _progressManager.Current.openedStage)
                    continue;

                stage.Show();
                stage.OnStageCompleted += OnStageCompleted;
                break;
            }
        }

        private void OnStageCompleted(LevelProgressStage stage)
        {
            stage.OnStageCompleted -= OnStageCompleted;
            if (stage.Id > _progressManager.Current.completedStage)
                _progressManager.Current.completedStage = stage.Id;
            
            _progressManager.Save();

            var message = new StageCompletedMessage
            {
                Stage = stage
            };
            _messageBroker.Trigger(ref message);

            if (stage.Next != null)
            {
                if (stage.Next.Id <= _progressManager.Current.openedStage)
                {
                    stage.Next.Show();
                    stage.Next.OnStageCompleted += OnStageCompleted;
                }
            }
            
            _cutsceneManager.StartCutscene(stage.Cutscene, () =>
            {
                if (stage.ShowClearedPopup)
                    _windowsSystem.CreateWindow<PopupWindow>()
                        .Setup(GameConst.AreaClearedText);
            });
        }
    }
}