﻿using System.Collections.Generic;
using Services;
using Services.DI;
using UnityEngine;
using UnityEngine.AI;

namespace Level.Citizens
{
    public class CitizensWaypointsGroup : MonoBehaviour
    {
        [SerializeField] private int _count;
        [SerializeField] private List<Transform> _waypoints;
        [SerializeField] private CitizenController _prefab;

        public Transform GetRandomWaypoint() => _waypoints.GetRandom();

        [ContextMenu("Spawn")]
        public void Spawn()
        {
            int remainingCount = _waypoints.Count;
            int requiredCount = Mathf.Min(_count, remainingCount);

            while (remainingCount > 0 && requiredCount > 0)
            {
                if (Random.Range(0, remainingCount) < requiredCount)
                {
                    var waypoint = _waypoints[remainingCount - 1];
                    if (NavMesh.SamplePosition(waypoint.position, out var hit, 10f, NavMesh.AllAreas))
                    {
                        var citizen = GameContainer.InstantiateAndResolve(_prefab, hit.position, waypoint.rotation);
                        citizen.transform.parent = transform;
                        citizen.Initialize(this);
                        requiredCount--;
                    }
                }

                remainingCount--;
            }
        }
    }
}