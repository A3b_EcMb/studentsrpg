﻿using UnityEngine;
using UnityEngine.AI;

namespace Level.Citizens
{
    public class CitizenController : MonoBehaviour
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        
        [SerializeField] private float _reachWaypointDistance;
        [SerializeField] private float _minWaitTime;
        [SerializeField] private float _maxWaitTime;
        [SerializeField] private Animator _animator;
        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private ObjectByDistanceChecker _distanceChecker;

        private Vector3 _prevPosition;
        private CitizensWaypointsGroup _waypointsGroup;
        private Transform _targetWaypoint;

        private float _waitTimer;

        public void Initialize(CitizensWaypointsGroup waypointsGroup)
        {
            _waypointsGroup = waypointsGroup;
            _distanceChecker.Enable();
        }

        private void Start()
        {
            _agent.enabled = false;
            _agent.enabled = true;
            Wait();
        }

        private void Update()
        {
            if (_waitTimer > 0f)
            {
                _waitTimer -= Time.deltaTime;
                if (_waitTimer <= 0f)
                    GetNewWaypoint();
                
                return;
            }
            
            var position = transform.position;
            var prevPosition = _prevPosition;
            _prevPosition = position;
            
            if (position == prevPosition)
            {
                Wait();
                return;
            }

            _agent.isStopped = false;
            float distance = Vector3.Distance(_targetWaypoint.position, position);
            if (distance > _reachWaypointDistance) return;

            Wait();
        }

        private void Wait()
        {
            _agent.isStopped = true;
            _waitTimer = Random.Range(_minWaitTime, _maxWaitTime);
            _animator.SetFloat(Speed, 0f);
        }

        [ContextMenu("Get new waypoint")]
        private void GetNewWaypoint()
        {
            _waitTimer = 0f;
            _targetWaypoint = _waypointsGroup.GetRandomWaypoint();
            _agent.isStopped = false;
            _agent.SetDestination(_targetWaypoint.position);
            _animator.SetFloat(Speed, 1f);
        }
    }
}