﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Invector;
using Invector.vCharacterController;
using Invector.vItemManager;
using Level.LevelProgress;
using LocalMessages;
using PlayerProgress;
using Services;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace Level.Respawn
{
    public class RespawnManager : MonoBehaviour
    {
        public vThirdPersonController Player { get; private set; }
        public vHealthController PlayerHealthController => Player;
        
        [SerializeField] private int _respawnTime;
        [SerializeField] private vThirdPersonController _playerPrefab;
        [SerializeField] private Transform _defaultSpawnPoint;

        [Inject] private LevelProgressManager _levelProgressManager;
        [Inject] private MessageBroker _messageBroker;
        [Inject] private PlayerProgressManager _playerProgressManager;
        [Inject] private WindowsSystem _windowsSystem;

        private Transform _spawnPoint;
        private CancellationTokenSource _tokenSource;
        
        public void Initialize()
        {
            GameContainer.InjectToInstance(this);

            _spawnPoint = _defaultSpawnPoint;
            foreach (var stage in _levelProgressManager.Stages)
            {
                if (!stage.Completed) break;
                if (stage.SpawnPoint == null) continue;
                if (!stage.UseAsStartSpawn) continue;

                _spawnPoint = stage.SpawnPoint;
            }
            
            _messageBroker.Subscribe<StageCompletedMessage>(OnStageCompleted);
            _messageBroker.Subscribe<ExitingToMenuMessage>(OnExitingToMenu);
            
            Respawn().Forget();
        }

        public void ConfirmRespawn()
        {
            _tokenSource = new CancellationTokenSource();
            WaitAndRespawn(_tokenSource.Token).Forget();
        }

        private void OnDestroy()
        {
            _messageBroker.Unsubscribe<StageCompletedMessage>(OnStageCompleted);
        }

        private async UniTask Respawn()
        {
            _tokenSource?.Cancel();
            if (Player != null)
            {
                Player.OnDead -= OnPlayerDead;
                Player.gameObject.SetActive(false);
                DestroyImmediate(Player.gameObject);
            }
            
            Player = Instantiate(_playerPrefab, _spawnPoint.position, _spawnPoint.rotation);
            Player.OnDead += OnPlayerDead;

            var itemManager = Player.GetComponent<vItemManager>();
            while (!itemManager.Initialized)
            {
                await UniTask.Yield();
            }
            
            foreach (var ownedItem in _playerProgressManager.Current.ownedItems)
            {
                var itemReference = new ItemReference(ownedItem.id);
                itemReference.amount = ownedItem.count;
                itemManager.AddItem(itemReference, true);
            }
        }

        private void OnStageCompleted(ref StageCompletedMessage message)
        {
            if (message.Stage.SpawnPoint != null)
                _spawnPoint = message.Stage.SpawnPoint;
        }

        private void OnPlayerDead(GameObject player)
        {
            FindNearestSpawnPoint();
            _playerProgressManager.ReadItemsFromInventory(Player.GetComponent<vItemManager>());
            _windowsSystem.CreateWindow<RespawnWindow>();
        }

        private void OnExitingToMenu(ref ExitingToMenuMessage message)
        {
            if (Player == null) return;
            var itemManager = Player.GetComponent<vItemManager>();
            if (itemManager == null) return;
            
            _playerProgressManager.ReadItemsFromInventory(itemManager);
        }

        private void FindNearestSpawnPoint()
        {
            _spawnPoint = _defaultSpawnPoint;
            float distance = Vector3.Distance(Player.transform.position, _spawnPoint.position);
            
            foreach (var stage in _levelProgressManager.Stages)
            {
                if (!stage.Completed) continue;
                if (stage.SpawnPoint == null) continue;

                float stageDistance = Vector3.Distance(stage.SpawnPoint.position, Player.transform.position);
                if (stageDistance > distance) continue;

                _spawnPoint = stage.SpawnPoint;
                distance = stageDistance;
            }
        }

        private async UniTask WaitAndRespawn(CancellationToken token)
        {
            await UniTask.Delay(_respawnTime, cancellationToken: token);
            if (token.IsCancellationRequested) return;
            Respawn().Forget();
        }
    }
}