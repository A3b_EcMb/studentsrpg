﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using LocalMessages;
using Services;
using Services.DI;
using Startup.GameStateMachine;
using UnityEngine;

namespace Startup
{
    public class Startup : MonoBehaviour
    {
        private static bool _initialized;
        
        [SerializeField] private List<InitializerBase> _startupInitializers;

        [Inject] private MessageBroker _messageBroker;

        private GameStateMachine.GameStateMachine _gameStateMachine;
        
        private void Awake()
        {
            if (_initialized)
            {
                Destroy(gameObject);
                return;
            }
            
            Initialize().Forget();
        }

        public async UniTask Play()
        {
            await _gameStateMachine.SwitchToState(GameStateType.Play);
        }

        public async UniTask GoToMenu()
        {
            var message = new ExitingToMenuMessage();
            _messageBroker.Trigger(ref message);
            
            await _gameStateMachine.SwitchToState(GameStateType.MainMenu);
        }

        private async UniTask Initialize()
        {
            GameContainer.Common = new Container();
            GameContainer.Common.Register(this);
            foreach (var initializer in _startupInitializers)
            {
                initializer.Initialize();
            }
            
            GameContainer.InjectToInstance(this);
            _gameStateMachine = new GameStateMachine.GameStateMachine();
            await _gameStateMachine.SwitchToState(GameStateType.MainMenu, true);
            
            _initialized = true;
        }
    }
}