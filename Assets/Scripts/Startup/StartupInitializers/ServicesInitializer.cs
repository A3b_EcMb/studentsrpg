﻿using GameSettings;
using Services;
using Services.DI;
using Services.SoundsSystem;
using UnityEngine;

namespace Startup.StartupInitializers
{
    public class ServicesInitializer : InitializerBase
    {
        [SerializeField] private SoundsSystem _soundsSystemPrefab;
        
        public override void Initialize()
        {
            var messageBroker = new MessageBroker();
            GameContainer.Common.Register(messageBroker);

            var gameSettings = new GameSettingsManager();
            GameContainer.Common.Register(gameSettings);
            
            var soundsSystem = GameContainer.InstantiateAndResolve(_soundsSystemPrefab);
            DontDestroyOnLoad(soundsSystem.gameObject);
            GameContainer.Common.Register(soundsSystem);
        }
    }
}