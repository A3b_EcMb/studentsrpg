﻿using PlayerProgress;
using Services.DI;
using UnityEngine;

namespace Startup.StartupInitializers
{
    public class ProgressInitializer : InitializerBase
    {
        [SerializeField] private PlayerLevelsConfig _levelsConfig;
        
        public override void Initialize()
        {
            var loginManager = GameContainer.Create<LoginManager>();
            GameContainer.Common.Register(loginManager);
            
            var progressManager = GameContainer.Create<PlayerProgressManager>();
            progressManager.Initialize(_levelsConfig);
            GameContainer.Common.Register(progressManager);
        }

        public override void Dispose()
        {
            GameContainer.Common.Resolve<PlayerProgressManager>().Save();
        }
    }
}