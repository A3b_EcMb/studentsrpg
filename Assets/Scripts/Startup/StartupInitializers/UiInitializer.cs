﻿using Services.DI;
using Services.WindowsSystem;
using Ui;
using UnityEngine;

namespace Startup.StartupInitializers
{
    public class UiInitializer : InitializerBase
    {
        [SerializeField] private UiRoot _uiRoot;
        [SerializeField] private LoadingScreen _loadingScreenPrefab;
        [SerializeField] private GameWindows _gameWindows;
        
        public override void Initialize()
        {
            DontDestroyOnLoad(_uiRoot);
            GameContainer.Common.Register(_uiRoot);

            var loadingScreen = Instantiate(_loadingScreenPrefab, _uiRoot.OverlayParent);
            GameContainer.Common.Register(loadingScreen);
            
            GameContainer.Common.Register(_gameWindows);
            
            var windowsSystem = GameContainer.Create<WindowsSystem>();
            GameContainer.Common.Register(windowsSystem);
            loadingScreen.Active = false;
        }
    }
}