﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Services.DI;
using Startup.GameStateMachine.States;

namespace Startup.GameStateMachine
{
    public enum GameStateType
    {
        MainMenu,
        Play
    }
    
    public class GameStateMachine
    {
        private readonly Dictionary<GameStateType, IGameState> _states = new()
        {
            { GameStateType.MainMenu, GameContainer.Create<MainMenuGameState>() },
            { GameStateType.Play, GameContainer.Create<PlayGameState>() },
        };

        private GameStateType _currentStateType;
        private IGameState _currentState;

        public async UniTask SwitchToState(GameStateType stateType, bool force = false)
        {
            if (_currentStateType == stateType && !force)
                return;

            if (_currentState != null)
                await _currentState.OnExit();

            _currentStateType = stateType;
            _currentState = _states[stateType];
            await _currentState.OnEnter();
        }
    }
}