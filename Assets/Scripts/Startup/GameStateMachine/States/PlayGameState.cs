﻿using Cysharp.Threading.Tasks;
using PlayerProgress;
using Services.DI;
using Services.SoundsSystem;
using Ui;
using UnityEngine.SceneManagement;

namespace Startup.GameStateMachine.States
{
    public class PlayGameState : IGameState
    {
        private const string SceneToLoad = "Level";
        private const string MainScene = "MainMenu";
        
        [Inject] private SoundsSystem _soundsSystem;
        [Inject] private LoadingScreen _loadingScreen;
        [Inject] private PlayerProgressManager _progressManager;
        
        public async UniTask OnEnter()
        {
            _loadingScreen.Active = true;
            _progressManager.CheckMinOpenedStage();
            
            await SceneManager.LoadSceneAsync(SceneToLoad);
            _soundsSystem.PlayMusic(MusicType.InGame);
            
            _loadingScreen.Active = false;
        }

        public async UniTask OnExit()
        {
            await SceneManager.LoadSceneAsync(MainScene);
        }
    }
}