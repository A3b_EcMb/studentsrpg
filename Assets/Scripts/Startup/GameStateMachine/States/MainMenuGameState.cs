﻿using Cysharp.Threading.Tasks;
using PlayerProgress;
using Services.DI;
using Services.SoundsSystem;
using Services.WindowsSystem;
using Ui;
using Ui.Windows;
using UnityEngine;

namespace Startup.GameStateMachine.States
{
    public class MainMenuGameState : IGameState
    {
        [Inject] private SoundsSystem _soundsSystem;
        [Inject] private WindowsSystem _windowsSystem;
        [Inject] private LoginManager _loginManager;
        [Inject] private PlayerProgressManager _progressManager;
        [Inject] private LoadingScreen _loadingScreen;
        
        public async UniTask OnEnter()
        {
            _soundsSystem.PlayMusic(MusicType.MainMenu);
            
            if (_loginManager.IsAuthorized)
            {
                _windowsSystem.CreateWindow<MainMenu>();
                return;
            }

            _loadingScreen.Active = true;
            var result = await _loginManager.LoginWithSavedCreds();
            _loadingScreen.Active = false;
            
            if (result.success)
            {
                _progressManager.Current.openedStage = result.stage;
                _windowsSystem.CreateWindow<MainMenu>();
                return;
            }

            _windowsSystem.CreateWindow<LoginWindow>();
        }

        public UniTask OnExit()
        {
            return UniTask.CompletedTask;
        }
    }
}