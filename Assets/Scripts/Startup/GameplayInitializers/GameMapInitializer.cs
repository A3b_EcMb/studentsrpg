﻿using DamageView;
using GameCamera;
using Level;
using Level.LevelProgress;
using Level.Respawn;
using Services;
using Services.DI;
using Services.WindowsSystem;
using Ui.Windows;
using UnityEngine;

namespace Startup.GameplayInitializers
{
    public class GameMapInitializer : InitializerBase
    {
        [SerializeField] private ObjectsOnDistanceDisabler _disabler;
        [SerializeField] private HpBarManager _hpBarManager;
        [SerializeField] private CameraCutsceneManager _cutsceneManager;
        [SerializeField] private LevelProgressManager _levelProgressManager;
        [SerializeField] private RespawnManager _respawnManager;
        [SerializeField] private RotateToCameraUpdater _rotateToCameraUpdater;
        
        public override void Initialize()
        {
            GameContainer.InGame.Register(_disabler);
            GameContainer.InGame.Register(_hpBarManager);
            GameContainer.InGame.Register(_cutsceneManager);
            GameContainer.InGame.Register(_levelProgressManager);
            GameContainer.InGame.Register(_respawnManager);
            GameContainer.InGame.Register(_rotateToCameraUpdater);
            
            _levelProgressManager.Initialize();
            _respawnManager.Initialize();
            
            var windowsSystem = GameContainer.Common.Resolve<WindowsSystem>();
            windowsSystem.CreateWindow<InGameUI>();

            var entryWindow = windowsSystem.CreateWindow<PopupWindow>();
            entryWindow.Setup(GameConst.EntryText);
        }

        public override void Dispose()
        {
            var windowsSystem = GameContainer.Common.Resolve<WindowsSystem>();
            windowsSystem.DestroyWindow<InGameUI>();
        }
    }
}