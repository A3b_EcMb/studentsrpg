﻿using System.Collections.Generic;
using Services.DI;
using UnityEngine;

namespace Startup
{
    public class LevelInitializer : MonoBehaviour
    {
        [SerializeField] private List<InitializerBase> _startupInitializers;

        private bool _initialized;
        
        private void Awake()
        {
            GameContainer.InGame = new Container();
            foreach (var initializer in _startupInitializers)
            {
                initializer.Initialize();
            }

            _initialized = true;
        }

        private void OnDestroy()
        {
            if (!_initialized) return;
            
            foreach (var initializer in _startupInitializers)
            {
                initializer.Dispose();
            }

            _initialized = false;
            GameContainer.InGame = null;
        }
    }
}