﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DamageView
{
    public class FloatingDamage : MonoBehaviour
    {
        private static Dictionary<int, string> _damagesTexts = new();
        
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private float _moveUpSpeed;
        [SerializeField] private float _showTime;

        private float _timer;
        private FloatingDamageManager _manager;

        public void Initialize(int damage, FloatingDamageManager manager)
        {
            if (!_damagesTexts.TryGetValue(damage, out string text))
            {
                text = $"-{damage.ToString()}";
                _damagesTexts[damage] = text;
            }

            _text.text = text;
            _timer = _showTime;
            _manager = manager;
        }

        private void Update()
        {
            _timer -= Time.deltaTime;
            transform.position += Vector3.up * (_moveUpSpeed * Time.deltaTime);

            if (_timer > 0f) return;
            
            _manager.Return(this);
        }
    }
}