﻿using LocalMessages;
using Services;
using Services.DI;
using UnityEngine;

namespace DamageView
{
    public class RotateToCamera : MonoBehaviour
    {
        [Inject] private RotateToCameraUpdater _updater;
        
        private Camera _camera;

        private void Start()
        {
            GameContainer.InjectToInstance(this);
            if (_updater != null)
                _updater.AddRotator(this);
        }

        private void OnDestroy()
        {
            if (_updater != null)
                _updater.RemoveRotator(this);
        }

        public void RotateTo(Transform cameraTransform)
        {
            transform.rotation = Quaternion.LookRotation(cameraTransform.forward);
        }
    }
}