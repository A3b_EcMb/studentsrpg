﻿using Invector;
using Services.DI;
using UnityEngine;

namespace DamageView
{
    public class EnemyHpBarRequester : MonoBehaviour
    {
        [SerializeField] private float _offset;
        [SerializeField] private vHealthController _target;
        
        [Inject] private HpBarManager _hpBarManager;

        private HpBar _hpBar;

        private void Start()
        {
            GameContainer.InjectToInstance(this);
            _target.onDead.AddListener(OnDead);
            
            if (gameObject.activeSelf && _hpBar == null)
                _hpBar = _hpBarManager.RequestHpBar(_target, _offset);
        }

        private void OnEnable()
        {
            if (_hpBarManager != null)
                _hpBar = _hpBarManager.RequestHpBar(_target, _offset);
        }

        private void OnDisable()
        {
            Return();
        }

        private void OnDead(GameObject arg0)
        {
            _target.onDead.RemoveListener(OnDead);
            Return();
            
            foreach (var collider in _target.GetComponentsInChildren<Collider>())
            {
                collider.enabled = false;
            }
        }

        private void Return()
        {
            if (_hpBar == null) return;
            
            _hpBarManager.ReturnHpBar(_hpBar);
            _hpBar = null;
        }

        [ContextMenu("Find")]
        private void Find()
        {
            _target = GetComponent<vHealthController>();
        }
    }
}