﻿using Invector;
using Services.Pools;
using UnityEngine;

namespace DamageView
{
    public class HpBarManager : MonoBehaviour
    {
        [SerializeField] private HpBar _prefab;

        public HpBar RequestHpBar(vHealthController target, float offset)
        {
            var instance = PrefabMonoPool<HpBar>.GetPrefabInstance(_prefab);
            instance.Initialize(target, offset);
            return instance;
        }

        public void ReturnHpBar(HpBar hpBar)
        {
            hpBar.Disable();
            PrefabMonoPool<HpBar>.ReturnInstance(hpBar);
        }
    }
}