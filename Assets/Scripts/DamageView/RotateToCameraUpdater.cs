﻿using System.Collections.Generic;
using LocalMessages;
using Services;
using Services.DI;
using UnityEngine;

namespace DamageView
{
    public class RotateToCameraUpdater : MonoBehaviour
    {
        [Inject] private MessageBroker _messageBroker;
        
        private Camera _camera;
        private List<RotateToCamera> _rotators;

        private void Awake()
        {
            _rotators = new List<RotateToCamera>();
        }

        private void Start()
        {
            GameContainer.InjectToInstance(this);
            _messageBroker.Subscribe<PlayerSpawnedMessage>(OnPlayerSpawned);
        }

        private void OnDestroy()
        {
            _messageBroker.Unsubscribe<PlayerSpawnedMessage>(OnPlayerSpawned);
        }

        private void OnPlayerSpawned(ref PlayerSpawnedMessage message)
        {
            _camera = Camera.main;
        }

        private void OnEnable()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            if (_camera == null)
            {
                _camera = Camera.main;
                if (_camera == null) return;
                return;
            }

            var cameraTransform = _camera.transform;
            foreach (var rotator in _rotators)
            {
                rotator.RotateTo(cameraTransform);
            }
        }

        public void AddRotator(RotateToCamera rotator)
        {
            _rotators.Add(rotator);
        }

        public void RemoveRotator(RotateToCamera rotator)
        {
            _rotators.Remove(rotator);
        }
    }
}