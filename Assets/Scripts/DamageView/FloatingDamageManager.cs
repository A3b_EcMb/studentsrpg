﻿using LocalMessages;
using Services;
using Services.DI;
using Services.Pools;
using UnityEngine;

namespace DamageView
{
    public class FloatingDamageManager : MonoBehaviour
    {
        [SerializeField] private FloatingDamage _prefab;

        [Inject] private MessageBroker _messageBroker;
        
        private void Start()
        {
            GameContainer.InjectToInstance(this);
            _messageBroker.Subscribe<ObjectDamagedMessage>(OnObjectDamaged);
        }

        private void OnObjectDamaged(ref ObjectDamagedMessage message)
        {
            var instance = PrefabMonoPool<FloatingDamage>.GetPrefabInstance(_prefab);
            instance.Initialize(Mathf.RoundToInt(message.Value.damageValue), this);
            instance.transform.position = message.Value.hitPosition;
        }

        public void Return(FloatingDamage instance)
        {
            PrefabMonoPool<FloatingDamage>.ReturnInstance(instance);
        }
    }
}