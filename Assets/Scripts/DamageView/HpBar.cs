﻿using System.Text;
using Invector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DamageView
{
    public class HpBar : MonoBehaviour
    {
        [SerializeField] private float _offset;
        [SerializeField] private Slider _fillImage;
        [SerializeField] private TextMeshProUGUI _hpText;

        private bool _hasTarget;
        private float _extraOffset;
        private vHealthController _target;

        private int _currentHp;
        private int _maxHp;

        private StringBuilder _stringBuilder;

        private void Awake()
        {
            _stringBuilder = new StringBuilder();
        }

        public void Initialize(vHealthController target, float offset)
        {
            _target = target;
            _hasTarget = true;
            _extraOffset = offset;
            
            _fillImage.value = _target.currentHealth / _target.maxHealth;
            
            int current = Mathf.FloorToInt(_target.currentHealth);
            int max = Mathf.FloorToInt(_target.maxHealth);
            
            _currentHp = current;
            _maxHp = max;

            _stringBuilder.Clear();
            _stringBuilder.Append(current);
            _stringBuilder.Append("/");
            _stringBuilder.Append(max);

            _hpText.text = _stringBuilder.ToString();
        }

        public void Disable()
        {
            _target = null;
            _hasTarget = false;
        }

        private void Update()
        {
            if (!_hasTarget) return;
                
            _fillImage.value = _target.currentHealth / _target.maxHealth;
            transform.position = _target.transform.position + Vector3.up * (_offset + _extraOffset);

            int current = Mathf.FloorToInt(_target.currentHealth);
            int max = Mathf.FloorToInt(_target.maxHealth);
            
            if (_currentHp == current && _maxHp == max) return;

            _currentHp = current;
            _maxHp = max;

            _stringBuilder.Clear();
            _stringBuilder.Append(current);
            _stringBuilder.Append("/");
            _stringBuilder.Append(max);

            _hpText.text = _stringBuilder.ToString();
        }
    }
}