﻿using System.Collections.Generic;
using UnityEngine.Serialization;

namespace Invector.vItemManager
{
    [vClassHeader("Check If Item Is Equipped", openClose = false)]
    public class vCheckItemIsEquipped : vMonoBehaviour
    {
        public vItemManager itemManager;
        public bool getInParent = true;
        [FormerlySerializedAs("itemChecks")]
        public List<CheckItemIDEvent> itemIDEvents;
        public List<CheckItemTypeEvent> itemTypeEvents;

        void Start()
        {
            if (itemManager) return;

            itemManager = getInParent
                ? GetComponentInParent<vItemManager>()
                : GetComponent<vItemManager>();
                
            if (!itemManager)
                itemManager = FindObjectOfType<vItemManager>();

            itemManager.onEquipItem.AddListener(CheckIsEquipped);
            itemManager.onUnequipItem.AddListener(CheckIsEquipped);
        }

        private void CheckIsEquipped(vEquipArea arg0, vItem arg1)
        {
            foreach (var check in itemIDEvents)
            {
                CheckItemID(check);
            }

            foreach (var check in itemTypeEvents)
            {
                CheckItemType(check);
            }
        }

        private void CheckItemID(CheckItemIDEvent check)
        {
            bool isEquipped = check._itemsID.Exists(t => itemManager.ItemIsEquipped(t));
            if (isEquipped == check.isEquipped) return;
            
            check.isEquipped = isEquipped;
            if (check.isEquipped)
                check.onIsItemEquipped.Invoke();
            else
                check.onIsItemUnequipped.Invoke();
        }

        private void CheckItemType(CheckItemTypeEvent check)
        {
            bool isEquipped = check.itemTypes.Exists(t => itemManager.ItemTypeIsEquipped(t));
            if (isEquipped == check.isEquipped) return;
            
            check.isEquipped = isEquipped;
            if (check.isEquipped)
                check.onIsItemEquipped.Invoke();
            else
                check.onIsItemUnequipped.Invoke();
        }

        [System.Serializable]
        public class CheckItemIDEvent
        {
            public string name;
            public List<int> _itemsID;
            public UnityEngine.Events.UnityEvent onIsItemEquipped, onIsItemUnequipped;
            internal bool isEquipped;
        }

        [System.Serializable]
        public class CheckItemTypeEvent
        {
            public string name;
            public List<vItemType> itemTypes;
            public UnityEngine.Events.UnityEvent onIsItemEquipped, onIsItemUnequipped;
            internal bool isEquipped;
        }
    }
}