﻿using System.Collections.Generic;
using UnityEngine;
namespace Invector.vCharacterController.AI
{
    [DisallowMultipleComponent]
    public class vSimpleMeleeAI_SphereSensor : MonoBehaviour
    {
        public Transform root;

        public List<Transform> targetsInArea;
        protected bool getFromDistance;
        protected float lastDetectionDistance;

        protected virtual void Start()
        {
            targetsInArea = new List<Transform>();
        }

        public virtual void AddTarget(Transform target)
        {
            if (!targetsInArea.Contains(target))
            {
                targetsInArea.Add(target);
            }
        }
    }
}

